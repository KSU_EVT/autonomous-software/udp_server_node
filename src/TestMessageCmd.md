
Terminal command to publish to control_msg topic

ros2 topic pub --once /control_topic voltron_msgs/msg/VcuControlMsg "{steer: 500, speed: 300, kp: 100}"

Terminal command to listen to status_msg topic

ros2 topic echo /status_topic

command to build for c++ debugger

colcon build --symlink-install --packages-select udp_server --cmake-args -DCMAKE_BUILD_TYPE=RelWithDebInfo

command to run for c++debugger

ros2 run --prefix 'gdbserver localhost:3000' udp_server udp_server_node