#include <ctime>
#include <iostream>
#include <string>
#include <boost/array.hpp>
#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include "voltron_msgs/msg/cone_bearings.hpp"
using boost::asio::ip::udp;

std::string make_daytime_string(){
    using namespace std;
    time_t now = time(0);
    return ctime(&now);
}

class udp_server{
    public:
        udp_server(boost::asio::io_context& io_context)
            : server_socket(io_context, udp::endpoint(boost::asio::ip::address::from_string("0.0.0.0"), 7777)) //init socket to listen on port 7777
        {
            start_receive();
        }
    private:
        void start_receive(){
            //receive_from writes to recv_buffer, assigns ip and port data to remote_endpoint, and does some token bullshittery
            server_socket.async_receive_from(
                boost::asio::buffer(recv_buffer),
                remote_endpoint,
                boost::bind( //bind calls the handle_receive member function with the parameters (this, error, bytes_transferred)
                    &udp_server::handle_receive,
                    this, //pointer to the current object, handle_receive technically needs the first parameter to be the object pointer, but is conventionally written as this.handle_receive()
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));

        }

        void handle_receive(const boost::system::error_code& error, std::size_t)
            {
                if (!error){
                    boost::shared_ptr<std::string> message(new std::string(make_daytime_string())); //make_daytime_string is temp function that creates send string
                    //send_to takes the message in a buffer, the vcu endpoint, and calls the handle_send function that does nothing at the moment
                    server_socket.async_send_to(boost::asio::buffer(*message),
                        vcu_endpoint,
                        boost::bind(&udp_server::handle_send,
                            this,
                            message,
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
                    
                    start_receive(); //after sending a message, the program waits again for another message
                }
            }

        void handle_send(
            boost::shared_ptr<std::string>,
            const boost::system::error_code&,
            std::size_t)
        {

        }

        udp::socket server_socket; //creates socket object named server_socket?
        udp::endpoint remote_endpoint; //creates endpoint object named remote_endpoint? Need multiple for each endpoint
        //need to set addresses and ports for each endpoint, currently program assigns data to endpoint with .async_receive_from() function
        boost::system::error_code ec;
        boost::asio::ip::address vcu_ip = boost::asio::ip::address::from_string(std::string("10.0.0.3"), ec);
        udp::endpoint vcu_endpoint = udp::endpoint(vcu_ip, 8888);

        boost::array<char, 1> recv_buffer;

};



int main(){
    try{
        boost::asio::io_context io_context;
        udp_server server(io_context);
        io_context.run();
    }
    catch (std::exception& e){
        std::cerr << e.what() <<std::endl;
    }

    return 0;
}