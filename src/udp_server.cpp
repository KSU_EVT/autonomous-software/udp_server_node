#include <stdint.h>

#include <chrono>
#include <ctime>
#include <iostream>
#include <string>
#include <voltron_msgs/msg/vcu_control_msg.hpp>

#include "voltron_msgs/msg/vcu_status_msg.hpp"
#include "voltron_msgs/msg/wheel_speed_msg.hpp"

// boost libraries
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/shared_ptr.hpp>

// ros libraries
#include <functional>
#include <memory>
#include <rclcpp/qos.hpp>
#include <string>

#include "rclcpp/executor.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

using boost::asio::ip::udp;

boost::system::error_code ec;

// ip addresses and endpoints
const boost::asio::ip::address vcu_ip =
    boost::asio::ip::address::from_string(std::string("10.0.0.3"), ec);
const boost::asio::ip::address odom_ip =
    boost::asio::ip::address::from_string(std::string("10.0.0.4"), ec);
const boost::asio::ip::address server_ip =
    boost::asio::ip::address::from_string(
        std::string("0.0.0.0"), ec);  // set to 0 to receive from all ips

const udp::endpoint vcu_endpoint = udp::endpoint(vcu_ip, 8888);
const udp::endpoint odom_endpoint = udp::endpoint(odom_ip, 8888);
const udp::endpoint server_endpoint = udp::endpoint(server_ip, 7777);

udp::endpoint remote_endpoint;  // only used as parameter for async receive
                                // function. returns remote IP

uint8_t recv_buffer[256];  // buffer for received udp data

struct evt_time_t {  // evt_time_t struct used for time stamps on vcu
  int32_t seconds;
  uint32_t microseconds;
};

struct vcu_status {  // struct for vcu status msg type
  int state = 0;
  float commanded_current = 0.0f;
  float commanded_drive_rpm = 0.0f;
  float commanded_steer = 0.0f;
  float reported_current = 0.0f;
  float reported_steer = 0.0f;
  float reported_rpm = 0.0f;
  float drive_batt_voltage = 0.0f;  //
  float steer_batt_voltage = 0.0f;  //
  evt_time_t timestamp;
};

struct vcu_control {  // struct for vcu control msg type
  float drive_current = 0.0f;
  float drive_rpm = 0.0f;
  float steering_revs = 0.0f;
};

struct rpm_sensors {
  float front_right;
  float front_left;
  evt_time_t timestamp;
};

rpm_sensors wheel_rpm;

// ros custom messages
auto control_msg = voltron_msgs::msg::VcuControlMsg();
auto status_msg = voltron_msgs::msg::VcuStatusMsg();
auto rpm_msg = voltron_msgs::msg::WheelSpeedMsg();

// alternative for handle_send function because shit was fucked
void please_kill_me(const boost::system::error_code&, std::size_t) { ; }

class RosUdpServer : public rclcpp::Node {
private:                      // private member functions and variables
  udp::socket server_socket;  // creates server_socket variable

  // creating fiber and publisher with message types
  rclcpp::Subscription<voltron_msgs::msg::VcuControlMsg>::SharedPtr
      control_subscriber;
  rclcpp::Publisher<voltron_msgs::msg::VcuStatusMsg>::SharedPtr
      status_publisher;
  rclcpp::Publisher<voltron_msgs::msg::WheelSpeedMsg>::SharedPtr
      wheel_speed_publisher;

  void start_receive() {
    server_socket.async_receive_from(      // starts async receive
        boost::asio::buffer(recv_buffer),  // receive buffer to write to
        remote_endpoint,  // endpoint to write to, can be used for verification
        boost::bind(  // call handle_receive func in this class, with error and
                      // bytes vars
            &RosUdpServer::handle_receive, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
  }

  void handle_receive(const boost::system::error_code& error, std::size_t len) {
    if (!error && (len == sizeof(vcu_status)) &&
        (remote_endpoint ==
         vcu_endpoint)) {  // will proceded to handle receive buffer if no error

      vcu_status recv = *((
          vcu_status*)recv_buffer);  // casts buffer to temp recv vcu_status msg

      // writes received data to status_msg
      status_msg.state = recv.state;
      status_msg.commanded_current = recv.commanded_current;
      status_msg.commanded_drive_rpm = recv.commanded_drive_rpm;
      status_msg.commanded_steer = recv.commanded_steer;
      status_msg.reported_current = recv.reported_current;
      status_msg.reported_steer = recv.reported_steer;
      status_msg.reported_rpm = recv.reported_rpm;
      status_msg.drive_batt_voltage = recv.drive_batt_voltage;  //
      status_msg.pc_batt_voltage = recv.steer_batt_voltage;     //
      status_msg.header.stamp.sec = recv.timestamp.seconds;
      status_msg.header.stamp.nanosec = (recv.timestamp.microseconds * 1000);

      // publish status_msg
      status_publisher->publish(status_msg);

      printf("===== VCU Message ====\n");
      printf("Commanded Current: %f\n", status_msg.commanded_current);
      printf("Commanded RPM: %f\n", status_msg.commanded_drive_rpm);
      printf("Commanded Steer: %f\n", status_msg.commanded_steer);
      printf("Reported Current: %f\n", status_msg.reported_current);
      printf("Reported Steer: %f\n", status_msg.reported_steer);
      printf("Reported RPM: %f\n", status_msg.reported_rpm);
      printf("Drive Battery Voltage %f\n", status_msg.drive_batt_voltage);  //
      printf("PC Battery Voltage %f\n", status_msg.pc_batt_voltage);
      printf("Timestamp (Secs : Nanosecs): %d : %d\n",
             status_msg.header.stamp.sec, status_msg.header.stamp.nanosec);
      printf("======================\n");

      // start_receive(); //start receiving again after packet (experiment with
      // removing?)

    } else if (!error && (len == sizeof(rpm_sensors)) &&
               (remote_endpoint == odom_endpoint)) {
      rpm_sensors recv = *((rpm_sensors*)recv_buffer);

      rpm_msg.fl_wheel_rpm = recv.front_left;
      rpm_msg.fr_wheel_rpm = recv.front_right;
      rpm_msg.header.stamp.sec = recv.timestamp.seconds;
      rpm_msg.header.stamp.nanosec = (recv.timestamp.microseconds * 1000);

      wheel_speed_publisher->publish(rpm_msg);

      printf("===== ODOM Message ====\n");
      printf("Right Wheel RPM: %f\n", rpm_msg.fr_wheel_rpm);
      printf("Left Wheel RPM: %f\n", rpm_msg.fl_wheel_rpm);
      printf("Timestamp (Secs : Nanosecs): %d : %d\n", rpm_msg.header.stamp.sec,
             rpm_msg.header.stamp.nanosec);
      printf("======================\n");

    } else if (error) {
      printf("Error Receiving Message\n");
    } else {
      printf("Message received not of right length\n");
    }

    start_receive();  // start receiving again after packet (experiment with
                      // removing?)
  }

  void topic_callback(const voltron_msgs::msg::VcuControlMsg&
                          control_msg) {  // callback for ros subscriber

    // print contents of message when received
    printf("Control Message Received\nCurrent: %f\nSteer: %f\n",
           control_msg.drive_current, control_msg.steering_revs);

    vcu_control send;
    // write ros message to send struct
    send.drive_current = control_msg.drive_current;
    send.drive_rpm = control_msg.drive_rpm;
    send.steering_revs = control_msg.steering_revs;

    server_socket.async_send_to(
        boost::asio::buffer((void*)&send, sizeof(send)),  // send send struct
        vcu_endpoint, please_kill_me);
  }

  // handle_send function that was brokey
  // void handle_send(
  //    vcu_control*,
  //    const boost::system::error_code&,
  //    std::size_t){
  //        //Nothing to see here
  //    }

public:
  RosUdpServer(boost::asio::io_context& io_context)  // udp server contstructor
      : Node("udp_server"), server_socket(io_context, server_endpoint) {
    // Create Subscriber on control_topic
    control_subscriber =
        this->create_subscription<voltron_msgs::msg::VcuControlMsg>(
            "vcu_control", 10,
            std::bind(&RosUdpServer::topic_callback, this,
                      std::placeholders::_1));

    // Create Publisher on status_topic
    status_publisher = this->create_publisher<voltron_msgs::msg::VcuStatusMsg>(
        "vcu_status", rclcpp::SensorDataQoS());  // change topic
    wheel_speed_publisher =
        this->create_publisher<voltron_msgs::msg::WheelSpeedMsg>(
            "odom_topic", rclcpp::SensorDataQoS());

    // start async udp packet receive
    start_receive();
  }

  ~RosUdpServer() { server_socket.close(); }
};

int main(int argc, char* argv[]) {
  // init ros (needs arguments for some reason)
  rclcpp::init(argc, argv);

  printf("Starting\n");

  boost::asio::io_context io_context;  // creates io_context
  std::shared_ptr<RosUdpServer> server = std::make_shared<RosUdpServer>(
      io_context);  // calls class and constructor
  rclcpp::executors::SingleThreadedExecutor exec;  // custom executor

  // Iinitiate shutdown sequence to properly release sockets on ctl c
  rclcpp::on_shutdown([] { std::terminate(); });

  exec.add_node(server);  // add node for server

  while (true) {
    io_context.run_one();  // call for udp
    exec.spin_once(
        std::chrono::nanoseconds(0));  // timeout prevents spin_once from
                                       // blocking without data to subscribe to
    // rclcpp::spin_some(server);
  }

  rclcpp::shutdown();
}
